const Task = require('../models/task.js')
const { success } = require('../helpers/handler')

const create = async (req, res, next) => {
  const { name, description, due_date } = req.body
  const owner = req.user

  const task = await new Task({
    owner,
    name,
    description,
    due_date
  })

  task
    .save()
    .then(data => {
      success(res, 201, data)
    })
    .catch(err => {
      next(err)
    })
}

const showAll = async (req, res, next) => {
  const page = parseInt(req.query.page, 10)
  await Task.paginate({ owner: req.user }, { page })
    .then(data => {
      success(res, 200, data)
    })
    .catch(err => {
      next(err)
    })
}

const sort = async (req, res, next) => {
  const page = parseInt(req.query.page, 10)
  const query = { owner: req.user }
  switch (req.query.filter) {
    case ('incomplete'):
      query.completion = false
      break

    case ('complete'):
      query.completion = true
      break

    case ('unimportance'):
      query.importance = false
      break

    case ('importance'):
      query.importance = true
      break
  }

  await Task.paginate(query, { page })
    .then(data => {
      success(res, 200, data)
    })
    .catch(err => {
      next(err)
    })
}

const update = async (req, res, next) => {
  await Task.findOneAndUpdate({ owner: req.user, _id: req.params._id }, req.body, { new: true })
    .then(data => {
      success(res, 200, data)
    })
    .catch(err => {
      next(err)
    })
}

const updateComplete = async (req, res, next) => {
  await Task.findByIdAndUpdate(req.params._id, {
    $set: { completion: req.body.completion }
  }, { new: true })
    .then(data => {
      success(res, 200, data)
    })
    .catch(err => {
      next(err)
    })
}

const updateImportance = async (req, res, next) => {
  await Task.findByIdAndUpdate(req.params._id, {
    $set: { importance: req.body.importance }
  }, { new: true })
    .then(data => {
      success(res, 200, data)
    })
    .catch(err => {
      next(err)
    })
}

const remove = async (req, res, next) => {
  await Task.deleteOne({ owner: req.user, _id: req.params._id })
    .then(data => {
      success(res, 200, data)
    })
    .catch(err => {
      next(err)
    })
}

module.exports = {
  create,
  showAll,
  sort,
  update,
  updateComplete,
  updateImportance,
  remove
}
