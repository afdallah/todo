const axios = require('axios')

exports.google = async (req, res) => {
  axios.get('https://oauth2.googleapis.com/tokeninfo', {
    headers: {
      Authorization: `Bearer ${req.query.access_token}`
    }
  })
    .then(response => {
      console.log(response.data)
      res.status(200).json({
        status: true,
        data: {
          email: response.data.email
        }
      })
    })
    .catch(err => {
      res.status(401).json({
        status: false,
        errors: err.response.data
      })
    })
}
