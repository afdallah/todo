const express = require('express')
// const morgan = require('morgan')
const cors = require('cors')
const swaggerUi = require('swagger-ui-express')
const cookieParser = require('cookie-parser')
const session = require('express-session')
const flash = require('express-flash')
const app = express()
process.log = {}

const swaggerDocument = require('./swagger.json')

// routers
const userRouter = require('./routes/user')
const taskRouter = require('./routes/task')
const viewRouter = require('./routes')

// models

// controllers
const userController = require('./controllers/user')

// middlewares
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(express.static('public'))
app.set('view engine', 'pug')
// app.use(morgan('dev'))
app.use(cors())

app.use(cookieParser('raHas1a'))
app.use(session({
  secret: 'slkjflksj',
  resave: true,
  saveUninitialized: true
}))
app.use(flash())

app.use(
  '/documentation',
  swaggerUi.serve,
  swaggerUi.setup(swaggerDocument, {
    customCss:
      '.topbar-wrapper { justify-content: center; text-align: center } .swagger-ui .topbar a { justify-content: center } .swagger-ui.swagger-container{ max-width: 100% } .swagger-ui { margin:auto; max-width: 70%;  } .wrapper .block { margin: 0 -20px }'
  })
)

// routers
app.use(viewRouter)
app.use('/api/v1/users', userRouter)
app.use('/api/v1/tasks', taskRouter)

// Activate account
app.get('/activate', userController.activate)

app.use(function (req, res, next) {
  res.status(404).send('The page you are looking for is not found')
})

// app.use(function (err, req, res, next) {
//   console.log(err.errors)
//   next(err)
// })

// Error handler
app.use(function (err, req, res, next) {
  const { statusCode = 400, message } = err

  res.status(statusCode).json({
    status: false,
    statusCode,
    message
  })
})

module.exports = app
