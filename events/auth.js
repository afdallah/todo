const EventEmitter = require('events')
const Auth = new EventEmitter()
process.log.users = {}

Auth.on('unauthorized', ({ _id, email, source }) => {
  if (!process.log.users[_id]) {
    (process.log.users[_id] = {
      email,
      count: 1
    })
  }

  process.log.users[_id].count++
  if (process.log.users[_id].count > 4) Auth.emit('fatal', email)
  console.log('final: ', process.log.users)
})

Auth.on('fatal', email => {
  console.log(`Sending email to ${email}`)
})

module.exports = Auth
