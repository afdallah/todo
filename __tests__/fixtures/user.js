const faker = require('faker')

const password = faker.internet.password()
exports.generateUser = function () {
  return {
    name: faker.name.firstName(),
    email: faker.internet.email(),
    password: password,
    password_confirmation: password
  }
}
