const multer = require('multer')

function upload () {
  return multer.single('photo')
}

module.exports = upload
