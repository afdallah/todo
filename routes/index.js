const express = require('express')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const router = express.Router()

const User = require('../models/user')
const mailer = require('../helpers/mailer')
const authController = require('../controllers/auth')
const moment = require('moment')
moment().format('MMMM Do YYYY, h:mm:ss a')

// Auth
router.post('/auth/google', authController.google)

// View Routes
router.get('/', (req, res) => {
  res.render('index', { title: 'Home' })
})

router.get('/reset-password', (req, res) => {
  res.render('reset-password', { title: 'Reset password' })
})

router.post('/submit-reset', async (req, res, next) => {
  try {
    const user = await User.findOne({ email: req.body.email })

    if (!user) {
      req.flash('error', 'Your email is not registered in our system')

      return res.redirect('back')
    }

    const token = jwt.sign({ _id: user._id }, process.env.JWT_SECRET_KEY, { expiresIn: '5m' })

    await mailer({
      to: user.email,
      subject: `Hai ${user.name}!, Here is your reset token.`,
      from: 'noreply@sukatodo.com',
      text: 'here is your token',
      html: `
        Click this link to reset <br /> <a href="${process.env.BASE_URL}/confirm-reset?token=${token}">Reset my password</a>
      `
    })

    res.render('reset-redirect', {})
  } catch (err) {
    next(err)
  }
})

router.get('/confirm-reset', async (req, res, next) => {
  const { token } = req.query

  try {
    const decoded = jwt.verify(token, process.env.JWT_SECRET_KEY)

    const user = await User.findById(decoded._id)
    if (!user) return res.redirect('/')

    res.render('confirm-reset', { user })
  } catch (err) {
    req.flash('error', 'Your token is expired, get new one!')
    res.redirect('/reset-password')
    next(err)
  }
})

router.post('/confirm-reset', async (req, res) => {
  const password = req.body.password[0]
  const { _id } = req.body

  const salt = bcrypt.genSaltSync(10)
  const encrypted_password = bcrypt.hashSync(password, salt)

  const user = await User.findByIdAndUpdate(_id, { $set: { encrypted_password } })

  res.render('success-reset', { user })
})

module.exports = router
