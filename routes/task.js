const express = require('express')
const router = express.Router()

const isAuthenticated = require('../middlewares/authenticate')
const task = require('../controllers/task')

// Create Task
router.post('/create', isAuthenticated, task.create)

// Show Task
router.get('/show', isAuthenticated, task.showAll)
router.get('/show/filter', isAuthenticated, task.sort)

// Update Task
router.put('/update/:_id', isAuthenticated, task.update)
router.put('/update/:_id/complete', isAuthenticated, task.updateComplete)
router.put('/update/:_id/importance', isAuthenticated, task.updateImportance)

// Delete Task
router.delete('/delete/:_id', isAuthenticated, task.remove)
module.exports = router
