const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')
mongoosePaginate.paginate.options = {
  limit: 10,
  sort: { createdAt: -1 }
}
const Schema = mongoose.Schema

const taskSchema = new Schema({
  name: {
    type: 'string',
    required: true
  },
  description: {
    type: 'string',
    required: true
  },
  due_date: {
    type: Date,
    required: true
  },
  importance: {
    type: 'boolean',
    default: false
  },
  completion: {
    type: 'boolean',
    default: false
  },
  owner: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  }
}, {
  versionKey: false,
  timestamps: true
})

taskSchema.plugin(mongoosePaginate)
const Task = mongoose.model('Task', taskSchema)

module.exports = Task
